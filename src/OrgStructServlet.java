import com.ibm.commons.runtime.Application;
import com.ibm.commons.runtime.Context;
import com.ibm.commons.runtime.RuntimeFactory;
import com.ibm.commons.runtime.impl.app.RuntimeFactoryStandalone;
import com.ibm.commons.util.io.json.JsonJavaObject;
import com.trinity.utils.NotesStructJsonBuilder;
import lotus.domino.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


public class OrgStructServlet extends HttpServlet {
    private String dbName;
    private Application application;
    private Context context;
    private static final String DB_NAME_PARAM = "db_name";

    private static Map<String, String> parseQueryParams(String query) throws ParseException {
        Map<String, String> queryMap = new HashMap<String, String>();
        String[] paramsArr = query.split("&");

        for (String param : paramsArr) {
            String[] kvArr = param.split("=", 2);
            if (kvArr.length != 2) {
                continue;
            }
            if (kvArr[0].equals("unid")) {
                Pattern unidPtr = Pattern.compile("[a-fA-F0-9]{32}");
                if (!unidPtr.matcher(kvArr[1]).matches()) {
                    throw new ParseException("Incorrect unid", 0);
                }
            }

            queryMap.put(kvArr[0], kvArr[1]);
        }

        return queryMap;
    }

    @Override
    public void init() throws ServletException {
        super.init();

        RuntimeFactory runtimeFactory = new RuntimeFactoryStandalone();
        application = runtimeFactory.initApplication(null);
        context = Context.init(this.application, null, null);
    }
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        NotesThread.sinitThread();

        if(application == null) {
            RuntimeFactory runtimeFactory = new RuntimeFactoryStandalone();
            application = runtimeFactory.initApplication(null);
        }

        if(context == null) {
            context = Context.init(application, null, null);
        }

        if(dbName == null || dbName.equals("")) {
            dbName = getInitParameter(DB_NAME_PARAM);
        }

        System.out.println("DEBUG: Database - " + dbName);

        JsonJavaObject resultJson = new JsonJavaObject();
        PrintWriter pw = resp.getWriter();

        resp.setContentType("application/json; charset=utf-8");

        try {
            String unid = parseQueryParams(req.getQueryString()).get("unid");

            Session session = NotesFactory.createSession();
            Database db = session.getDatabase("", dbName);
            Document settings = db.getView("Settings").getFirstDocument();

            NotesStructJsonBuilder jsonBuilder = new NotesStructJsonBuilder(db, settings);

            resultJson = jsonBuilder.getStructAsJson(unid);
            resultJson.put("result", "success");
        } catch (NotesException e) {
            resultJson.put("result", "failed");
            resultJson.put(
                    "errorMessage",
                    "Notes Exception - Failed to retrieve data from Notes. Check request params"
            );
            e.printStackTrace();
        } catch (ParseException e) {
            resultJson.put("result", "failed");
            resultJson.put("errorMessage", "Query Parse Exception - Incorrect unid parameter");
            e.printStackTrace();
        } catch (Exception e) {
            resultJson.put("result", "failed");
            resultJson.put("errorMessage", "Unknown exception: " + e.getMessage());
            e.printStackTrace();
        } finally {
            pw.println(resultJson.toString());
            NotesThread.stermThread();
        }
    }

    @Override
    public void destroy() {
        Context.destroy(this.context);
        Application.destroy(this.application);
    }
}
