let LotusOrgTreeApi = (function () {
    let m = {};

    function getPersonCard(fields) {
        let html = "",
            optRows = [],
            labels = {
                profile: "\u041F\u0440\u043E\u0444\u0430\u0439\u043B",
                communities: "\u0421\u043E\u043E\u0431\u0449\u0435\u0441\u0442\u0432\u0430",
                blogs: "\u0411\u043B\u043E\u0433\u0438",
                forums: "\u0424\u043E\u0440\u0443\u043C\u044B",
                wikis: "\u0412\u0438\u043A\u0438",
                files: "\u0424\u0430\u0439\u043B\u044B",
                dogears: "\u0417\u0430\u043A\u043B\u0430\u0434\u043A\u0438",
                activities: "\u041E\u043F\u0435\u0440\u0430\u0446\u0438\u0438",
                groupwareEmail: "\u041A\u043E\u043B\u043B\u0435\u043A\u0441\u0442\u0438\u0432\u043D\u0430\u044F \u043F\u043E\u0447\u0442\u0430",
                department: "\u0414\u0435\u043F\u0430\u0440\u0442\u0430\u043C\u0435\u043D\u0442",
                floor: "\u042D\u0442\u0430\u0436",
                bldg: "\u0421\u0442\u0440\u043E\u0435\u043D\u0438\u0435",
                street: "\u0423\u043B\u0438\u0446\u0430",
                locality: "\u0413\u043E\u0440\u043E\u0434",
                region: "\u0420\u0435\u0433\u0438\u043E\u043D",
                country: "\u0421\u0442\u0440\u0430\u043D\u0430",
                postalCode: "\u041F\u043E\u0447\u0442\u043E\u0432\u044B\u0439 \u0438\u043D\u0434\u0435\u043A\u0441",
                sendEmail: "\u041E\u0442\u043F\u0440\u0430\u0432\u0438\u0442\u044C \u043F\u0438\u0441\u044C\u043C\u043E",
                downloadVCard: "\u0421\u043A\u0430\u0447\u0430\u0442\u044C \u0432\u0438\u0437\u0438\u0442\u043D\u0443\u044E \u043A\u0430\u0440\u0442\u043E\u0447\u043A\u0443",
                addons: "\u0414\u043E\u043F\u043E\u043B\u043D\u0438\u0442\u0435\u043B\u044C\u043D\u044B\u0435 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044F"
            },
            optional = [
                {label: "groupwareEmail", value: fields.groupwareEmail},
                {label: "department", value: fields.department},
                {label: "floor", value: fields.floor},
                {label: "bldg", value: fields.building}
            ],
            required = [
                "profilePage",
                "photo",
                "displayName",
                "services",
                "jobResp",
                "email"
            ];

        for(let fName of required) {
            if(!fields.hasOwnProperty(fName) || fields[fName] === null) {
                console.error("Cannot render profile card. The field " + fName + " is not provided.");
                if(fields.hasOwnProperty("profilePage") && fields.hasOwnProperty("displayName"))
                    return `<span class="person-entry f14"><a href="${fields.profilePage}">${fields.displayName}</a></span>`;
                else
                    return "";
            }
        }

        html += `<span class="person-entry f14"><a href="${fields.profilePage}">${fields.displayName}</a>`;
        html += `<div class="person-card"><div class="person-card-header">`;
        html += `<table class="profile-links"><tr>`;
        html += `<td><a href="${fields.services.profiles}">${labels.profile}</a></td>`;
        html += `<td><a href="${fields.services.communities}">${labels.communities}</a></td>`;
        html += `<td><a href="${fields.services.blogs}">${labels.blogs}</a></td>`;
        html += `<td><a href="${fields.services.forums}">${labels.forums}</a></td>`;
        html += `</tr><tr>`;
        html += `<td><a href=${fields.services.wikis}>${labels.wikis}</a></td>`;
        html += `<td><a href="${fields.services.files}">${labels.files}</a></td>`;
        html += `<td><a href="${fields.services.dogears}">${labels.dogears}</a></td>`;
        html += `<td><a href="${fields.services.activities}">${labels.activities}</a></td>`;
        html += `</tr></table></div><div class="person-card-content">`;
        html += `<img src="${fields.photo}"/><div class="person-fields">`;
        html += `<span class="display-name"><a href="${fields.profilePage}">${fields.displayName}</a></span>`;
        html += `<div class="other-fields">`;
        html += `<span><b>${fields.jobResp}</b></span>`;

        if(fields.hasOwnProperty("address")) {
            optional.concat([
                {label: "street", value: fields.address.streetAddress},
                {label: "locality", value: fields.address.locality},
                {label: "region", value: fields.address.region},
                {label: "country", value: fields.address.countryName},
                {label: "postalCode", value: fields.address.postalCode}
            ]);
        }

        for(let field of optional) {
            if(typeof(field.value) !== 'undefined' && field.value !== null) {
                optRows.push(labels[field.label] + ": " + field.value);
            }
        }

        html += `<span>${optRows.join(" | ")}</span>`;

        html += `<span><a href="mailto:${fields.email}">${fields.email}</a></span>`;
        html += `</div></div></div>`;
        html += `<div class="person-card-footer"><ul class="actions-list">`;
        html += `<li><a href="mailto:${fields.email}">${labels.sendEmail}</a></li>`;
        html += `<li class="additional-actions"><a href="javascript:void(0);">`;
        html += `${labels.addons}<span class="icon-arrow-bottom"> </span>`;
        html += `<ul class="dropdown"><li>`;
        html += `<a href="mailto:${fields.email}">${labels.sendEmail}</a>`;
        html += `</li><li><a href="javascript:void(0)">${labels.downloadVCard}</a>`;
        html += `</li></ul></a></li></ul></div></div></span>`;

        return html;
    }

    function getEmployeeList(employees) {
        let rows = employees.map(employee => {
            let rowHtml = "";

            rowHtml += `<tr><td class="profileEntry">`;
            rowHtml += `<img src="${employee["photo"]}">`;

            rowHtml += getPersonCard(employee);

            rowHtml += `<p>${employee["jobResp"]}</p></td>`;
            if(employee.hasOwnProperty("actualLocation") && employee["actualLocation"] !== null) {
                rowHtml += `<td>${employee["actualLocation"]}</td>`;
            }
            if(employee.hasOwnProperty("externalPhone") && employee["externalPhone"] !== null)
                rowHtml += `<td><span class="f14">${employee["externalPhone"]}</span></td>`;

            if(employee.hasOwnProperty("internalPhone") && employee["internalPhone"] !== null)
                rowHtml += `<td><span class="f14">${employee["internalPhone"]}</span></td></tr>`;
            return rowHtml;
        });

        return rows.join("");
    }

    m.SearchEmployees = function($place, params={}) {
        let paramsArr = [],
            paramToField = {
                "Any": "search",
                "By name": "displayName",
                "By email": "email",
                "By phone": "internalPhone",
                "By staff": "jobResp"
            };

        for(let param in params) {
            if(params.hasOwnProperty(param))
                paramsArr.push(paramToField[param] + "=" + params[param]);
        }

        let query = "/servlet/ProfilesSearch?" + paramsArr.join("&");

        return fetch(query)
            .then(resp => resp.json())
            .then(json => {
                $place.html(getEmployeeList(json.employees))
            });
    };


    m.TreeLoader = class {
        constructor(lotusAgentUrl) {
            this._depLoader = new Map();
            this._lotusAgentUrl = lotusAgentUrl;
        }

        get(unid) {
            let def;

            if (!this._depLoader.has(unid)) {
                def = $.Deferred();
                this._depLoader.set(unid, def);

                fetch(this._lotusAgentUrl + unid)
                    .then(resp => resp.json())
                    .then(
                        struct => {
                            def.resolveWith(struct);
                        },
                        err => {
                            console.error("Failed to fetch data of oganization with id: " + unid);
                            def.rejectWith(err);
                        }
                    );
            } else {
                def = this._depLoader.get(unid);
            }

            return def;
        }
    };

    m.TreeWidget = class {
        constructor(params = {}) {
            let required = ["$tree", "$employees", "$breadcrumbs"];

            if (params.hasOwnProperty("loader")) {
                if(! params["loader"] instanceof m.TreeLoader) {
                    throw new Error("Widget initialization failed. Loader parameter is not instanceof TreeLoader");
                }
                this._depLoader = params["loader"];
            } else {
                this._depLoader = new m.TreeLoader(params["lotusAgentUrl"]);
                required.push("lotusAgentUrl");
            }

            for (let elem of required) {
                if (!params.hasOwnProperty(elem) || !elem) {
                    throw new Error("Widget initialization failed. " + elem + " param is not defined.");
                }
                this["_" + elem] = params[elem];
            }

            this._unid = null;
        }

        load(unid) {
            this._unid = unid;
            return this._depLoader.get(this._unid);
        }

        render() {
            if (!this._unid) {
                throw new Error("Cannot render tree - call load(unid) first");
            }

            let self = this;

            return this.load(this._unid)
                .then(function () {
                    console.log("response: ", this);
                    self._$tree.html("");
                    self._createTree(self._$tree, this["department"]);
                    self._refreshEmployees(self._unid);
                });
        }

        _open(unid) {
            let self = this,
                $place = $("#" + unid);

            return self._depLoader.get(unid)
                .then(function () {
                    self._createTree($place, this["department"]);
                    self._refreshEmployees(unid);
                    $place
                        .children()
                        .closest(".js-so-substruct")
                        .find("span.tree-item-icon")
                        .addClass("open");
                });
        }

        _close(unid) {
            let $li = $("#" + unid);

            $li.children(".js-so-tree").remove();
            $li.children()
                .closest(".js-so-substruct")
                .find("span.tree-item-icon")
                .removeClass("open");

            this._refreshEmployees(unid);
        }

        _refreshEmployees(unid) {
            let self = this;

            return this._depLoader.get(unid)
                .then(function () {
                    let employees = this["employees"];

                    self._$employees.html(getEmployeeList(employees));
                    self._refreshBreadcrumbs(unid);
                })
        }

        _refreshBreadcrumbs(unid) {
            let $place = this._$breadcrumbs;
            $place.html("");

            let createBreadcrumb = (dep) => $("<a></a>")
                .attr("href", "javascript:void(0)")
                .html(dep.name)
                .on("click", () => {
                    this._$employees.html("");
                    $("<iframe></ifrmae>")
                        .attr("frameborder", 0)
                        .css({width: "100%", height: "100%"})
                        .attr("src", dep.href)
                        .appendTo(this._$employees);
                });

            return this._depLoader.get(unid)
                .then(function () {
                    let curDep = this["department"],
                        departments = curDep["parent-departments"].map(createBreadcrumb);
                    departments.push(createBreadcrumb(curDep));
                    console.log("breadcrumbs 1: ", departments);
                    for(let i = 1; i < departments.length; i+=2)
                        departments.splice(i, 0, $("<span class='delimiter'></span>"));
                    console.log("breadcrumbs 2: ", departments);
                    $place.append(departments);
                });
        }

        _createTree($place, department) {
            let $ul = $place.find(".js-so-tree"),
                $li;

            if ($ul.length === 0)
                $ul = $("<ul></ul>").addClass("js-so-tree").appendTo($place);

            for (let dep of department["child-departments"]) {
                let unid = dep.unid,
                    $employees = this._$employees,
                    links = [];

                $li = $("<li></li>").attr("id", unid);

                if (dep["has-child"])
                    links.push(
                        $("<a></a>")
                            .addClass("js-so-substruct")
                            .html("<span class='tree-item-icon'></span>")
                            .on("click", e => this[!$(e.target).hasClass("open") ? "_open" : "_close"](unid))
                    );

                links.push(
                    $("<a href='javascript:void(0);'></a>")
                        .html("<span class='tree-item-icon struct'></span>")
                        .on("click", function () {
                            $employees.html("");
                            $("<iframe></ifrmae>")
                                .attr("frameborder", 0)
                                .css({width: "100%", height: "100%"})
                                .attr("src", dep.href)
                                .appendTo($employees);
                        })
                );


                links.push(
                    $("<a></a>")
                        .html(dep["name"])
                        .attr("href", "javascript:void(0)")
                        .on("click", e => this._refreshEmployees(unid))
                );

                $li.append(links).appendTo($ul);
            }
        }
    };

    return m;
})();