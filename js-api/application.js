(function () {
    function getBrowserEngine() {
        let engines = [
                {engine: "WebKit", regex: /AppleWebKit\/[\d.]+/},
                {engine: "Gecko", regex: /Gecko\/[\d.]+/},
                {engine: "Blink", regex: /Chrome\/[\d.]+/},
                {engine: "Presto", regex: /Opera\/[\d.]+/},
                {engine: "Trident", regex: /Trident\/[\d.]+/}
            ],
            ua = navigator.userAgent,
            engine = null;

        for (let e of engines) {
            if (ua.search(e.regex) !== -1) {
                engine = e.engine;
                break;
            }
        }

        return engine;
    }

    $(document).ready(() => {
        let $scrollStyle = $(".scroll-style-1"),
            $soTab = $(".js-so-tab"),
            api = LotusOrgTreeApi,
            tree = new api.TreeWidget({
                $tree: $("#tree"),
                $employees: $("#emplList"),
                $breadcrumbs: $("#depmnt_breadcrumbs"),
                loader: new api.TreeLoader("/servlet/OrgStructServlet?unid=")
            });


        $soTab.each((i, tab) => {
            let $tab = $(tab),
                unid = $tab.attr("id");

            tree.load(unid);

            if ($tab.hasClass("active")) {
                tree.render();
            }

            return tree;
        });


        $soTab.on("click", function (e) {
            let $tab = $(e.target),
                $li = $tab.parent(),
                unid = $li.attr("id");

            $li.siblings().removeClass("active");
            $li.addClass("active");
            tree.load(unid);
            tree.render();
        });

        $(".search-input-wrapper .dropdown-toggle")
            .on("click", function() {
                $(this).parent().toggleClass("open");
            });

        $(".search-input-wrapper .dropdown-menu")
            .on("focusout", function(e) {
                e.preventDefault();
                $(".search-input-wrapper .input-group-btn").removeClass("open");
            });

        $(".search-input-wrapper .dropdown-menu li a")
            .on("click", function(e) {
                e.preventDefault();
                $(".search-input-wrapper .js-selected-value").text($(this).text());
            });

        $("form").submit(function(e) {
            e.preventDefault();
            window.history.back();
        });

        $(".btn-search").on("click", function(e) {
            let query = {},
                $loader = $(".loader");
            query[$(".js-selected-value").text()] = $(".js-search-query").val();
            console.log(query);
            $("#emplList").html("");
            $loader.addClass("loading");
            api.SearchEmployees($("#emplList"), query).then(function() {
                $loader.removeClass("loading");
            });
        });


        if ($scrollStyle.length > 0 && getBrowserEngine() !== "WebKit") {
            $scrollStyle.each(function () {
                let obj = $(this).get(0);
                new PerfectScrollbar(obj);
            });
        }
    });
}());